resource "random_pet" "rg_name" {
  prefix = var.resource_group_name_prefix
}

resource "azurerm_resource_group" "aksrg" {
  location = var.resource_group_location
  name = random_pet.rg_name.id
}

resource "random_pet" "azure_kubernetes_cluster_name" {
  prefix = "cluster"
}

resource "random_pet" "azure_kubernetes_cluster_dns_prefix" {
  prefix = "dns"
}

resource "azurerm_kubernetes_cluster" "k8s" {
  location = azurerm_resource_group.aksrg.location
  name = random_pet.azure_kubernetes_cluster_name.id 
  resource_group_name = azurerm_resource_group.aksrg.name 
  dns_prefix = random_pet.azure_kubernetes_cluster_dns_prefix.id 

  identity {
    type = "SystemAssigned"
  }
  default_node_pool {
    name = "agentpool"
    vm_size =  "Standard_F2"
    node_count = var.node_count
  }
  linux_profile {
    admin_username = var.username
    ssh_key {
      key_data = file("${path.module}/ssh-key/terraform-azure.pub")
    }
  }
  network_profile {
    network_plugin = "kubenet"
    load_balancer_sku = "standard"
  }
}

