variable "resource_group_location" {
  type = string
  default = "eastus2"
}

variable "resource_group_name_prefix" {
  type = string 
  default = "rg"
}

variable "node_count" {
  type = number 
  default = 2
}

variable "username" {
  type = string 
  default = "azureadmin"
}